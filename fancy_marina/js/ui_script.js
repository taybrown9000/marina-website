$(document).ready(function() {
    // fullpage init
    $('#fullpage').fullpage({
        anchors: ['home', 'reporting'],
        easing: "easeOutQuint",
        slidesNavigation: true,
        scrollingSpeed: 900,
        loopHorizontal: false,
    });

    // home page button actions
    $(".btn.home").click(function() {
        if ($(this).text() == "Maintenance") {
            $.fn.fullpage.moveSlideLeft();
        }
        else if ($(this).text() == "Reporting") {
            $.fn.fullpage.moveSlideRight();
        }
    });

    // form swapping
    $('.btn.maint.menu').click(function(e) {
        // stop page from loading after function completes
        e.preventDefault();

        var btn = $(this);
        // btn.addClass('btn.maint.menu:hover');

        var inputForm = $(".maint-input");
        var notify = $(".notify");
        var newForm;

        // notification text
        switch (btn.text()) {
            case "Marina":
                newForm = "forms/marina.html";
                notify.text("Marina Table selected");
                break;

            case "Owner":
                newForm = "forms/owner.html";
                notify.text("Owner Table selected");
                break;

            case "Marina Slip":
                newForm = "forms/marina_slip.html";
                notify.text("Marina Slip Table selected");
                break;

            case "Service Category":
                newForm = "forms/service_category.html";
                notify.text("Service Category Table selected");
                break;

            case "Service Request":
                newForm = "forms/service_request.html";
                notify.text("Service Request Table selected");
                break;

            default:
                break;
        } 

        inputForm.hide("slide", loadForm);

        function loadForm() {
            inputForm.load(newForm);
            inputForm.show("slide", { direction:"left" }, "normal");
        }
    });

    // form validation
    function validateForm() {
        console.log("fuck");
    //     var notify = $('.notify');

    //     var validator = $('#maint-form').validate({
    //         debug: false,

    //         rules: {
    //             marina_num: {
    //                 required: true,
    //                 digits: true 
    //             },
    //             slip_ID: {
    //                 required: true,
    //                 digits: true 
    //             },
    //             owner_num: {
    //                 required: true,
    //                 digits: true 
    //             },
    //             category_num: {
    //                 required: true,
    //                 digits: true 
    //             },
    //             service_ID: {
    //                 required: true,
    //                 digits: true 
    //             }
    //         },

    //         messages: {
    //             marina_num: "shit"//function() {
    //             //     notify.addClass($('.notify.error'));
    //             //     notify.text("The Marina Number field is blank");
    //             // }
    //         },

    //         submitHandler: function(form) {
    //         // do other stuff for valid form
    //         $.post('../php/maintenance.php', $('#maint-form').serialize());
    //     }
    // });
    }

    // init h2 variables
    var maintHeader = $("#h2-maint");
    var reportHeader = $("#h2-report");
    maintHeader.hide();
    reportHeader.hide();

    // check which page the user is on to show/hide the header 
    setInterval(function() {
        var hash = window.location.hash;

        if (hash == "#home") {
            maintHeader.show("slide", 300);
        } else {
            maintHeader.hide("slide", 100);
        }

        if (hash == "#home/2") {
            reportHeader.show("slide", {direction: "right" }, 300);
        } else {
            reportHeader.hide("slide", {direction: "right" }, 100);
        }
    }, 50);

    // INIT

    // load marina form
    $(".maint-input").load("forms/marina.html");
    $(".notify").text("Marina Table selected");

});