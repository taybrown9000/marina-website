<!doctype html>
<html class="no-js" lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Search Service Requests by Category Description</title>
  <link rel="stylesheet" href="../css/foundation.css" />
  <link rel="stylesheet" href="../css/app.css" />
  <script src="../js/vendor/modernizr.js"></script>
</head>

<?php 

  $open_radio = 'checked';
  $closed_radio = 'unchecked';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

  $errors = array();

  if (isset($_POST['search-btn'])) {

    $cat_desc = trim($_POST['cat_desc']);
    $selected_radio = $_POST['radioGroup'];

    if ($selected_radio == 'Open') {
      $open_radio = 'checked';
      $status = 'Open';
    }
    else if ($selected_radio == 'Closed') {
      $closed_radio = 'checked';
      $status = 'Closed';
    }

    // connect to database
    require('mysqli_connect.php');

    // make the query
    $q = "SELECT SLIP_ID, DESCRIPTION, STATUS FROM SERVICE_REQUEST R, SERVICE_CATEGORY C
          WHERE R.CATEGORY_NUM = C.CATEGORY_NUM
          AND CATEGORY_DESCRIPTION = '$cat_desc'
          AND STATUS = '$status'";

    $r = @mysqli_query($dbc, $q);

    // check if the result was successful
    if (mysqli_num_rows($r) == 0) {
      $errors[] = "Could not find that Boat Type in the database.";
      $color = "red";
    }
    else {
      $success = "Data successfully retrieved!";
      $color = "green";
    }
  }

  mysqli_close($dbc);
}

?>

<body>

  <a class="pic" href="../index.html"><img src="../img/marina.jpg" /></a>
  <a class="pic" href="../index.html"><img href="../index.html" src="../img/marina.jpg" class="right"/></a>
  <header>
    <h1>Brown Marina</h1>
  </header>

  <h2>Search Service Requests<br />
	by Category Description</h2>
  <div class="page table">

    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id="myForm" data-abide>

    <div class="row">
      <div class="small-10 columns small-centered">

      <!-- output textarea -->
      <div class="row">
        <div class="small-10 small-centered columns">
          <label class="inline">Output
            <textarea id="textarea" readonly class="error" style="color: <?php echo $color; ?>">
              <?php 
            // check if the operation was successful
              if (isset($success)) {
                echo "- $success";
              } else {
                foreach ($errors as $msg) {
                  echo "- $msg";
                }
              }
              ?>
            </textarea></label>
          </div>
        </div>



          <div class="small-3 columns">
            <label for="openSwitch">Status: </label>
            <div class="row collapse">
              <div class="small-6 columns">
                <input type="radio" name="radioGroup" value="Open" <?PHP echo $open_radio; ?> />Open 
              </div>
              <div class="small-6 columns">
              <input type="radio" name="radioGroup" value="Closed" <?PHP echo $closed_radio; ?> />Closed 
              </div>
            </div>
          </div>

          <div class="small-9 columns">

            <!-- label -->
            <div class="small-3 columns">
              <label for="cat-label" class="right">Category Description:</label>
            </div>

            <!-- text input -->
            <div class="row collapse">
              <div class="small-9 columns">

                <div class="small-9 columns">

                  <input type="text" id="cat-label" name="cat_desc"
                  value="<?php echo $_POST['cat_desc']; ?>" required>

                  <small class="error">Please enter a valid Boat Type</small>
                </div>

                <!-- postfix button -->
                <div class="small-3 columns">
                  <button class="button postfix" name="search-btn">Search</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>

      <div class="row">
        <div class="small-6 small-centered columns">
          <table>

            <thead>
              <tr>
                <th>Slip ID</th>
                <th width="250px">Description</th>
                <th width="100px">Status</th>
              </tr>
            </thead>

            <tbody>
              <?php 

                if (isset($success)) {

                  while ($row = @mysqli_fetch_assoc($r)) {
                    echo "<tr>";
                    echo "<td>".$row['SLIP_ID']."</td>";
                    echo "<td>".$row['DESCRIPTION']."</td>";
                    echo "<td>".$row['STATUS']."</td>";
                    echo "</tr>";
                  }
                }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

    <a href="index.html" class="prev">Go Back</a>

    <footer>
      &copy; Taylor
      <br />Last Updated 10/15/14
    </footer>


    <script src="../js/vendor/jquery.js"></script>
    <script src="../js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>

  </html>
