<!doctype html>
<html class="no-js" lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Owner Report</title>
  <link rel="stylesheet" href="../css/foundation.css" />
  <link rel="stylesheet" href="../css/app.css" />
  <script src="../js/vendor/modernizr.js"></script>
</head>

<body>

  <a class="pic" href="../index.html"><img src="../img/marina.jpg" /></a>
  <a class="pic" href="../index.html"><img href="../index.html" src="../img/marina.jpg" class="right"/></a>
  <header>
    <h1>Brown Marina</h1>
  </header>

  <h2>Owner Report</h2>

  <div class="page table">

  <div class="row">
  <div class="large-12 columns">
      <table>

        <thead>
          <tr>
            <th>Owner Number</th>
            <th>Last Name</th>
            <th>First Name</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Zip Code</th>
          </tr>
        </thead>

        <tbody>
          <?php 
               // connect to database
          require('mysqli_connect.php');

            // make the query
          $q = @mysqli_query($dbc, "SELECT * FROM OWNER");

          while ($row = @mysqli_fetch_assoc($q)) {
            echo "<tr>";
            echo "<td>".$row['OWNER_NUM']."</td>";
            echo "<td>".$row['LAST_NAME']."</td>";
            echo "<td>".$row['FIRST_NAME']."</td>";
            echo "<td>".$row['ADDRESS']."</td>";
            echo "<td>".$row['CITY']."</td>";
            echo "<td>".$row['STATE']."</td>";
            echo "<td>".$row['ZIP']."</td>";
            echo "</tr>";
          }

            // close db connection
          mysqli_close($dbc);
          ?>

        </tbody>
      </table>
  </div>
  </div>
  </div>

  <a href="index.html" class="prev home">Go Back</a>

  <footer class="service">
    &copy; Taylor
    <br />Last Updated 10/15/14
  </footer>


  <script src="../js/vendor/jquery.js"></script>
  <script src="../js/foundation.min.js"></script>
  <script>
    $(document).foundation();

    $(document).ready(function() {

      $('#reset-btn').click(function() {
        $(this).closest('form').find("input[type=text], textarea").val("");
      });


    });
  </script>
</body>

</html>
