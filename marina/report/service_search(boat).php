<!doctype html>
<html class="no-js" lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Search Services by Boat</title>
  <link rel="stylesheet" href="../css/foundation.css" />
  <link rel="stylesheet" href="../css/app.css" />
  <script src="../js/vendor/modernizr.js"></script>
</head>

<?php 

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

  $errors = array();

  if (isset($_POST['search-btn'])) {

    $boat_type = trim($_POST['boat_type']);

    // connect to database
    require('mysqli_connect.php');

    // make the query
    $q = "SELECT SERVICE_ID, C.CATEGORY_DESCRIPTION, DESCRIPTION, STATUS, EST_HOURS, 
          SPENT_HOURS, NEXT_SERVICE_DATE FROM SERVICE_REQUEST S, MARINA_SLIP M, SERVICE_CATEGORY C
          WHERE M.SLIP_ID = S.SLIP_ID AND S.CATEGORY_NUM = C.CATEGORY_NUM 
          AND M.BOAT_TYPE = '$boat_type'";

    $r = @mysqli_query($dbc, $q);

    // check if the result was successful
    if (mysqli_num_rows($r) == 0) {
      $errors[] = "Could not find that Boat Type in the database.";
      $color = "red";
    }
    else {
      $success = "Data successfully retrieved!";
      $color = "green";
    }
  }

  mysqli_close($dbc);
}

?>

<body>

  <a class="pic" href="../index.html"><img src="../img/marina.jpg" /></a>
  <a class="pic" href="../index.html"><img href="../index.html" src="../img/marina.jpg" class="right"/></a>
  <header>
    <h1>Brown Marina</h1>
  </header>

  <h2>Search Services by Boat</h2>
  <div class="page table">

    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id="myForm" data-abide>

      <!-- output textarea -->
      <div class="row">
        <div class="small-12 columns">
          <label class="inline">Output
            <textarea id="textarea" readonly class="error" style="color: <?php echo $color; ?>">
              <?php 
            // check if the operation was successful
              if (isset($success)) {
                echo "- $success";
              } else {
                foreach ($errors as $msg) {
                  echo "- $msg";
                }
              }
              ?>
            </textarea></label>
          </div>
        </div>


        <div class="row">
          <div class="small-12 columns">

            <!-- label -->
            <div class="small-3 columns">
              <label for="num-label" class="right inline">Boat Type:</label>
            </div>

            <!-- text input -->
            <div class="row collapse">
              <div class="small-9 columns">

                <div class="small-9 columns">

                  <input type="text" id="num-label" name="boat_type"
                  value="<?php echo $_POST['boat_type']; ?>" required>

                  <small class="error">Please enter a valid Boat Type</small>
                </div>

                <!-- postfix button -->
                <div class="small-3 columns">
                  <button class="button postfix" name="search-btn">Search</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>

      <div class="row">
        <div class="small-12 columns">
          <table>

            <thead>
              <tr>
                <th>Service ID</th>
                <th>Category Description</th>
                <th>Description</th>
                <th>Status</th>
                <th>Estimated Hours</th>
                <th>Spent Hours</th>
                <th>Next Service Date</th>
              </tr>
            </thead>

            <tbody>
              <?php 

                if (isset($success)) {

                  while ($row = @mysqli_fetch_assoc($r)) {
                    echo "<tr>";
                    echo "<td>".$row['SERVICE_ID']."</td>";
                    echo "<td>".$row['CATEGORY_DESCRIPTION']."</td>";
                    echo "<td>".$row['DESCRIPTION']."</td>";
                    echo "<td>".$row['STATUS']."</td>";
                    echo "<td>".$row['EST_HOURS']."</td>";
                    echo "<td>".$row['SPENT_HOURS']."</td>";
                    echo "<td>".$row['NEXT_SERVICE_DATE']."</td>";
                    echo "</tr>";
                  }
                }
              ?>

            </tbody>
          </table>
        </div>
      </div>
    </div>


    <a href="index.html" class="prev">Go Back</a>

    <footer>
      &copy; Taylor
      <br />Last Updated 10/15/14
    </footer>


    <script src="../js/vendor/jquery.js"></script>
    <script src="../js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>

  </html>
