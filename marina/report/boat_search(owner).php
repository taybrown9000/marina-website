<!doctype html>
<html class="no-js" lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Search Boats by Owner</title>
  <link rel="stylesheet" href="../css/foundation.css" />
  <link rel="stylesheet" href="../css/app.css" />
  <script src="../js/vendor/modernizr.js"></script>
</head>

<?php 

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

  $errors = array();

  if (isset($_POST['search-btn'])) {

    $owner_num = trim($_POST['owner_num']);

    // connect to database
    require('mysqli_connect.php');

                    // make the query
    $q = "SELECT O.OWNER_NUM, FIRST_NAME, LAST_NAME, BOAT_NAME, BOAT_TYPE 
    FROM MARINA_SLIP M, OWNER O
    WHERE M.OWNER_NUM = O.OWNER_NUM AND O.OWNER_NUM = '$owner_num'";
    $r = @mysqli_query($dbc, $q);

    // check if the result was successful
    if (mysqli_num_rows($r) == 0) {
      $errors[] = "Could not find that Owner Number in the database.";
      $color = "red";
    }
    else {
      $success = "Data successfully retrieved!";
      $color = "green";
    }
  }

  mysqli_close($dbc);
}

?>

<body>

  <a class="pic" href="../index.html"><img src="../img/marina.jpg" /></a>
  <a class="pic" href="../index.html"><img href="../index.html" src="../img/marina.jpg" class="right"/></a>
  <header>
    <h1>Brown Marina</h1>
  </header>

  <h2>Search Boats by Owner</h2>
  <div class="page form">

    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id="myForm" data-abide>

      <!-- output textarea -->
      <div class="row">
        <div class="small-12 columns">
          <label class="inline">Output
            <textarea id="textarea" readonly class="error" style="color: <?php echo $color; ?>">
              <?php 
            // check if the operation was successful
              if (isset($success)) {
                echo "- $success";
              } else {
                foreach ($errors as $msg) {
                  echo "- $msg";
                }
              }
              ?>
            </textarea></label>
          </div>
        </div>


        <div class="row">
          <div class="small-12 columns">

            <!-- label -->
            <div class="small-3 columns">
              <label for="num-label" class="right inline">Owner Number:</label>
            </div>

            <!-- text input -->
            <div class="row collapse">
              <div class="small-9 columns">

                <div class="small-9 columns">

                  <input type="text" id="num-label" name="owner_num" maxlength="4"
                  value="<?php echo $_POST['owner_num']; ?>" required pattern="alpha_numeric">

                  <small class="error">Please enter a valid Owner Number</small>
                </div>

                <!-- postfix button -->
                <div class="small-3 columns">
                  <button class="button postfix" name="search-btn">Search</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>

      <div class="row">
        <div class="small-12 columns">
          <table>

            <thead>
              <tr>
                <th>Owner Number</th>
                <th width="110px">First Name</th>
                <th >Last Name</th>
                <th width="100px">Boat Name</th>
                <th width="120px">Boat Type</th>
              </tr>
            </thead>

            <tbody>
              <?php 

                if (isset($success)) {

                  while ($row = @mysqli_fetch_assoc($r)) {
                    echo "<tr>";
                    echo "<td>".$row['OWNER_NUM']."</td>";
                    echo "<td>".$row['FIRST_NAME']."</td>";
                    echo "<td>".$row['LAST_NAME']."</td>";
                    echo "<td>".$row['BOAT_NAME']."</td>";
                    echo "<td>".$row['BOAT_TYPE']."</td>";
                    echo "</tr>";
                  }
                }
              ?>

            </tbody>
          </table>
        </div>
      </div>
    </div>


    <a href="index.html" class="prev home">Go Back</a>

    <footer>
      &copy; Taylor
      <br />Last Updated 10/15/14
    </footer>


    <script src="../js/vendor/jquery.js"></script>
    <script src="../js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>

  </html>
