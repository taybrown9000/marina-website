<!doctype html>
<html class="no-js" lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Owner Table Maintenance</title>
  <link rel="stylesheet" href="../css/foundation.css" />
  <link rel="stylesheet" href="../css/app.css" />
  <script src="../js/vendor/modernizr.js"></script>
</head>

<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // initialize error array
  $errors = array();

    // retrieve function
  if (isset($_POST['retrieve-btn'])) {

    // get owner number
    $owner_num = trim($_POST['owner_num']);

    // connect to database
    require('mysqli_connect.php');

    // make the query
    $q = "SELECT * FROM OWNER WHERE OWNER_NUM = '$owner_num'";
    $r = @mysqli_query($dbc, $q);

      // check if the result was successful
    if (mysqli_num_rows($r) == 1) {
      $row = mysqli_fetch_array($r, MYSQLI_ASSOC);

      $last_name = $row['LAST_NAME'];
      $first_name = $row['FIRST_NAME'];
      $address = $row['ADDRESS'];
      $city = $row['CITY'];
      $state = $row['STATE'];
      $zipcode = $row['ZIP'];

      $success = "Data successfully retrieved!\n" .
      "Owner Number: " . $owner_num . "\n" .
      "Last Name: " . $last_name . "\n" .
      "First Name: " . $first_name . "\n" .
      "Address: " . $address . "\n" .
      "City: " . $city . "\n" .
      "State: " . $state . "\n" .
      "Zip Code: " . $zipcode;
      $color = "green";
    }
    else {
      $errors[] = "That Owner Number doesn't seem to exist...Did you type it in correctly?";
      $color = "red";
    }
  } // end of retrieve function

  // insert function
  if (isset($_POST['insert-btn'])) {

    require('verify_owner.php');

    // if everything's OK
    if (empty($errors)) {

      // connect to database
      require('mysqli_connect.php');

      //check if owner number exists already
      $q = "SELECT OWNER_NUM FROM OWNER WHERE OWNER_NUM = '$owner_num'";
      $r = @mysqli_query($dbc, $q);

      // if the row is not found, continue
      if (mysqli_num_rows($r) == 0) {

        $q = "INSERT INTO OWNER (OWNER_NUM, LAST_NAME, FIRST_NAME, ADDRESS, CITY, STATE, ZIP) VALUES
        ('$owner_num', '$last_name', '$first_name','$address', '$city', '$state', '$zipcode')";

      // run the query
        $r = @mysqli_query($dbc, $q);

      // if it ran OK
        if ($r) {
          $success = "Data successfully inserted!\n" .
          "Owner Number: " . $owner_num . "\n" .
          "Last Name: " . $last_name . "\n" .
          "First Name: " . $first_name . "\n" .
          "Address: " . $address . "\n" .
          "City: " . $city . "\n" .
          "State: " . $state . "\n" .
          "Zip Code: " . $zipcode;

          $color = "green";

        } else {
          $errors[] = "There was an error inserting the data into the database.\nWe apologize for any inconvenience.";
          $color = "red";
        }

      // thing already exists
      } 
    } else {
      if (empty($errors)) {
          $errors[] = "The Owner Number you've entered already exists in the database.";
          $color = "red";
      }
    }
  } // end of insert function

  // modify function
  if (isset($_POST['modify-btn'])) {

    require('verify_owner.php');

    // if everything's OK
    if (empty($errors)) {

      // connect to database
      require('mysqli_connect.php');

      //check if owner number exists already
      $q = "SELECT OWNER_NUM FROM OWNER WHERE OWNER_NUM = '$owner_num'";
      $r = @mysqli_query($dbc, $q);

      // if the row is found, continue
      if (mysqli_num_rows($r) == 1) {

        $q = "UPDATE OWNER SET LAST_NAME='$last_name', FIRST_NAME='$first_name', ADDRESS='$address', 
        CITY='$city', STATE='$state', ZIP='$zipcode'
        WHERE OWNER_NUM = '$owner_num'";  

      // run the query
        $r = @mysqli_query($dbc, $q);

      // if it ran OK
        if ($r) {
          $success = "Data successfully modified!\n" .
          "Owner Number: " . $owner_num . "\n" .
          "Last Name: " . $last_name . "\n" .
          "First Name: " . $first_name . "\n" .
          "Address: " . $address . "\n" .
          "City: " . $city . "\n" .
          "State: " . $state . "\n" .
          "Zip Code: " . $zipcode;

          $color = "green";

        } else {
          $errors[] = "There was an error replacing the data in the database.\nWe apologize for any inconvenience.";
          $color = "red";
        }

      // the data doesn't exist in the table
      } else {
        $errors[] = "The data was not found in the table...Did you type the Owner Number in correctly?";
        $color = "red";
      }   
    } 
  } // end of modify function


  // delete function
  if (isset($_POST['delete-btn'])) {

    // get owner number
    $owner_num = trim($_POST['owner_num']);

    // connect to database
    require('mysqli_connect.php');

    // make the query
    $q = "SELECT * FROM OWNER WHERE OWNER_NUM = '$owner_num'";
    $r = @mysqli_query($dbc, $q);

    // check if the result was successful
    if (mysqli_num_rows($r) == 1) {

      $q = "DELETE FROM OWNER WHERE OWNER_NUM = '$owner_num'";
      $r = @mysqli_query($dbc, $q);

      // if it ran OK
      if ($r) {
        $success = "Data successfully deleted!\n" .
        "Owner Number: " . $owner_num;

        $color = "green";

      } else {
        $errors[] = "There was an error deleting the data from the database.\nWe apologize for any inconvenience.";
        $color = "red";
      }

    // not found 
    } else {
      $errors[] = "The specified Owner Number was not found in the database. Unable to delete.";
        $color = "red";
    }
  }

    // close db connection
  mysqli_close($dbc);
}

?>

<body>

  <a class="pic" href="../index.html"><img src="../img/marina.jpg" /></a>
  <a class="pic" href="../index.html"><img href="../index.html" src="../img/marina.jpg" class="right"/></a>
  <header>
    <h1>Brown Marina</h1>
  </header>

  <h2>Owner Table</h2>
  <div class="page form">

    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id="myForm" data-abide>

      <!-- output textarea -->
      <div class="row">
        <div class="small-12 columns">
          <label class="inline">Output
            <textarea id="textarea" readonly class="error" style="color: <?php echo $color; ?>">
              <?php 
            // check if the operation was successful
              if (isset($success)) {
                echo "- $success";
              } else {
                foreach ($errors as $msg) {
                  echo "- $msg\n";
                }
              }
              ?>
            </textarea></label>
          </div>
        </div>


        <div class="row">
          <div class="small-12 columns">

            <!-- label -->
            <div class="small-3 columns">
              <label for="num-label" class="right inline">Owner Number:</label>
            </div>

            <!-- text input -->
            <div class="row collapse">
              <div class="small-9 columns">

                <div class="small-9 columns">

                  <input type="text" id="num-label" name="owner_num" maxlength="4"
                  value="<?php echo $_POST['owner_num']; ?>" required pattern="alpha_numeric">

                  <small class="error">Please enter a valid Owner Number</small>
                </div>

                <!-- postfix button -->
                <div class="small-3 columns">
                  <button class="button postfix" name="retrieve-btn">Retrieve</button>
                </div>
              </div>
            </div>
          </div>
        </div>


        <div class="row">

          <div class="small-3 columns">
            <label for="lname-label" class="right inline">Last Name:</label>
          </div>

          <div class="small-9 columns">
            <input type="text" name="last_name" value="<?php echo $last_name; ?>"
            id="lname-label">
          </div>

        </div>

        <div class="row">

          <div class="small-3 columns">
            <label for="fname-label" class="right inline">First Name:</label>
          </div>

          <div class="small-9 columns">
            <input type="text" name="first_name" value="<?php echo $first_name; ?>"
            id="fname-label">
          </div>

        </div>

        <div class="row">

          <div class="small-3 columns">
            <label for="address-label" class="right inline">Address:</label>
          </div>

          <div class="small-9 columns">
            <input type="text" name="address" value="<?php echo $address; ?>"
            id="address-label">
          </div>

        </div>

        <div class="row">

          <div class="small-3 columns">
            <label for="city-label" class="right inline">City:</label>
          </div>

          <div class="small-9 columns">
            <input type="text" name="city" value="<?php echo $city; ?>"
            id="city-label">
          </div>

        </div>

        <div class="row">

          <div class="small-3 columns">
            <label for="state-label" class="right inline">State:</label>
          </div>

          <div class="small-9 columns">
            <input type="text" name="state" maxlength="2" value="<?php echo $state; ?>"
            id="state-label">
          </div>

        </div>

        <div class="row">

          <div class="small-3 columns">
            <label for="zip-label" class="right inline">Zip Code:</label>
          </div>

          <div class="small-9 columns">
            <input type="text" name="zipcode" maxlength="5" value="<?php echo $zipcode; ?>"
            id="zip-label">
          </div>

        </div>

        <div class="row">

          <div class="small-3 columns">
            <button class="form" name="insert-btn">Insert</button>
          </div>

          <div class="small-3 columns">
            <button class="form" name="modify-btn">Modify</button>
          </div>

          <div class="small-3 columns">
            <button class="form" name="delete-btn" onclick="return confirm('Are you sure you want to Delete?')">
              Delete
            </button>
          </div>

          <div class="small-3 columns">
            <button class="button alert" id="reset-btn" type="button">Reset</button>
          </div>

        </div>
      </form>

    </div>

    <a href="index.html" class="prev">Go Back</a>

    <footer>
      &copy; Taylor
      <br />Last Updated 10/15/14
    </footer>


    <script src="../js/vendor/jquery.js"></script>
    <script src="../js/foundation.min.js"></script>
    <script>
      $(document).foundation();

      $(document).ready(function() {

        $('#reset-btn').click(function() {
          $(this).closest('form').find("input[type=text], textarea").val("");
        });


      });
    </script>
  </body>

  </html>
