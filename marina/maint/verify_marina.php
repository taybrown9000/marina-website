<?php
	$color = "red";

    // get marina number
    $marina_num = trim($_POST['marina_num']);

    // Check for a name:
    if (empty($_POST['name'])) {
      $errors[] = "The Name field is blank";
    } else {
      $name = trim($_POST['name']);
    }

    // Check for an address:
    if (empty($_POST['address'])) {
      $errors[] = "The Address field is blank";
    } else {
      $address = trim($_POST['address']);
    }

    // Check for a city:
    if (empty($_POST['city'])) {
      $errors[] = "The City field is blank";
    } else {
      $city = trim($_POST['city']);
    }

    // Check for a state:
    if (empty($_POST['state'])) {
      $errors[] = "The State field is blank";
    } else {
      $state = trim($_POST['state']);
    }

    // Check for a zipcode:
    if (empty($_POST['zipcode'])) {
      $errors[] = "The Zip Code field is blank";
    } else {
      $zipcode = trim($_POST['zipcode']);
    }
?>