<!doctype html>
<html class="no-js" lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Marina Slip Maintenance</title>
  <link rel="stylesheet" href="../css/foundation.css" />
  <link rel="stylesheet" href="../css/app.css" />
  <script src="../js/vendor/modernizr.js"></script>
</head>

<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // initialize error array
  $errors = array();

    // retrieve function
  if (isset($_POST['retrieve-btn'])) {

    // get slip ID
    $slip_ID = trim($_POST['slip_ID']);

    // connect to database
    require('mysqli_connect.php');

    // make the query
    $q = "SELECT * FROM MARINA_SLIP WHERE SLIP_ID = '$slip_ID'";
    $r = @mysqli_query($dbc, $q);

      // check if the result was successful
    if (mysqli_num_rows($r) == 1) {
      $row = mysqli_fetch_array($r, MYSQLI_ASSOC);

      $marina_num = $row['MARINA_NUM'];
      $slip_num = $row['SLIP_NUM'];
      $length = $row['LENGTH'];
      $rental_fee = $row['RENTAL_FEE'];
      $boat_name = $row['BOAT_NAME'];
      $boat_type = $row['BOAT_TYPE'];
      $owner_num = $row['OWNER_NUM'];

      $success = "Data successfully retrieved!\n" .
      "Slip ID: " . $slip_ID . "\n" .
      "Marina Number: " . $marina_num . "\n" .
      "Slip Number: " . $slip_num . "\n" .
      "Length: " . $length . "\n" .
      "Rental Fee: " . $rental_fee . "\n" .
      "Boat Name: " . $boat_name . "\n" .
      "Boat Type: " . $boat_type . "\n" .
      "Owner Number: " . $owner_num;
      $color = "green";
    }
    else {
      $errors[] = "That Slip ID doesn't seem to exist...Did you type it in correctly?";
      $color = "red";
    }
  } // end of retrieve function

  // insert function
  if (isset($_POST['insert-btn'])) {

    require('verify_marina_slip.php');

    // if everything's OK
    if (empty($errors)) {

      // connect to database
      require('mysqli_connect.php');

      //check if marina number exists already
      $q = "SELECT SLIP_ID FROM MARINA_SLIP WHERE SLIP_ID = '$slip_ID'";
      $r = @mysqli_query($dbc, $q);

      // if the row is not found, continue
      if (mysqli_num_rows($r) == 0) {

      $q = "INSERT INTO MARINA_SLIP (SLIP_ID, MARINA_NUM, SLIP_NUM, LENGTH, RENTAL_FEE, 
            BOAT_NAME, BOAT_TYPE, OWNER_NUM) VALUES ('$slip_ID', '$marina_num', '$slip_num', 
            '$length', '$rental_fee', '$boat_name', '$boat_type', '$owner_num')";

      // run the query
        $r = @mysqli_query($dbc, $q);

      // if it ran OK
        if ($r) {
          $success = "Data successfully inserted!\n" .
          "Slip ID: " . $slip_ID . "\n" .
          "Marina Number: " . $marina_num . "\n" .
          "Slip Number: " . $slip_num . "\n" .
          "Length: " . $length . "\n" .
          "Rental Fee: " . $rental_fee . "\n" .
          "Boat Name: " . $boat_name . "\n" .
          "Boat Type: " . $boat_type . "\n" .
          "Owner Number: " . $owner_num;

          $color = "green";

        } else {
          $errors[] = "There was an error inserting the data into the database.\nWe apologize for any inconvenience.";
          $color = "red";
        }

      // thing already exists
      } 
    } else {
      if (empty($errors)) {
          $errors[] = "The Slip ID you've entered already exists in the database.";
          $color = "red";
      }
    }
  } // end of insert function

  // modify function
  if (isset($_POST['modify-btn'])) {

    require('verify_marina_slip.php');

    // if everything's OK
    if (empty($errors)) {

      // connect to database
      require('mysqli_connect.php');

      //check if marina number exists already
      $q = "SELECT SLIP_ID FROM MARINA_SLIP WHERE SLIP_ID = '$slip_ID'";
      $r = @mysqli_query($dbc, $q);

      // if the row is found, continue
      if (mysqli_num_rows($r) == 1) {

        $q = "UPDATE MARINA_SLIP SET SLIP_ID='$slip_ID', MARINA_NUM='$marina_num', 
        SLIP_NUM='$slip_num', LENGTH='$length', RENTAL_FEE='$rental_fee', BOAT_NAME='$boat_name',
        BOAT_TYPE='$boat_type', OWNER_NUM='$owner_num'
        WHERE SLIP_ID = '$slip_ID'";  

      // run the query
        $r = @mysqli_query($dbc, $q);

      // if it ran OK
        if ($r) {
          $success = "Data successfully modified!\n" .
          "Slip ID: " . $slip_ID . "\n" .
          "Marina Number: " . $marina_num . "\n" .
          "Slip Number: " . $slip_num . "\n" .
          "Length: " . $length . "\n" .
          "Rental Fee: " . $rental_fee . "\n" .
          "Boat Name: " . $boat_name . "\n" .
          "Boat Type: " . $boat_type . "\n" .
          "Owner Number: " . $owner_num;

          $color = "green";

        } else {
          $errors[] = "There was an error replacing the data in the database.\nWe apologize for any inconvenience.";
          $color = "red";
        }

      // the data doesn't exist in the table
      } else {
        $errors[] = "The data was not found in the table...Did you type the Slip ID in correctly?";
        $color = "red";
      }   
    } 
  } // end of modify function


  // delete function
  if (isset($_POST['delete-btn'])) {

    // get marina number
    $slip_ID = trim($_POST['slip_ID']);

    // connect to database
    require('mysqli_connect.php');

    // make the query
    $q = "SELECT * FROM MARINA_SLIP WHERE SLIP_ID = '$slip_ID'";
    $r = @mysqli_query($dbc, $q);

    // check if the result was successful
    if (mysqli_num_rows($r) == 1) {

      $q = "DELETE FROM MARINA_SLIP WHERE SLIP_ID = '$slip_ID'";
      $r = @mysqli_query($dbc, $q);

      // if it ran OK
      if ($r) {
        $success = "Data successfully deleted!\n" .
        "Slip ID: " . $slip_ID;

        $color = "green";

      } else {
        $errors[] = "There was an error deleting the data from the database.\nWe apologize for any inconvenience.";
        $color = "red";
      }

    // not found 
    } else {
      $errors[] = "The specified Slip ID was not found in the database. Unable to delete.";
        $color = "red";
    }
  }

    // close db connection
  mysqli_close($dbc);
}

?>

<body>

  <a class="pic" href="../index.html"><img src="../img/marina.jpg" /></a>
  <a class="pic" href="../index.html"><img href="../index.html" src="../img/marina.jpg" class="right"/></a>
  <header>
    <h1>Brown Marina</h1>
  </header>

  <h2>Marina Slip Table</h2>
  <div class="page form">

    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id="myForm" data-abide>

      <!-- output textarea -->
      <div class="row">
        <div class="small-12 columns">
          <label class="inline">Output
            <textarea id="textarea" readonly class="error" style="color: <?php echo $color; ?>">
              <?php 
            // check if the operation was successful
              if (isset($success)) {
                echo "- $success";
              } else {
                foreach ($errors as $msg) {
                  echo "- $msg\n";
                }
              }
              ?>
            </textarea></label>
          </div>
        </div>


        <div class="row">
          <div class="small-12 columns">

            <!-- label -->
            <div class="small-3 columns">
              <label for="id-label" class="right inline">Slip ID:</label>
            </div>

            <!-- text input -->
            <div class="row collapse">
              <div class="small-9 columns">

                <div class="small-9 columns">

                  <input type="text" id="id-label" name="slip_ID"
                  value="<?php echo $_POST['slip_ID']; ?>" required pattern="number">

                  <small class="error">Please enter a valid Slip ID</small>
                </div>

                <!-- postfix button -->
                <div class="small-3 columns">
                  <button class="button postfix" name="retrieve-btn">Retrieve</button>
                </div>
              </div>
            </div>
          </div>
        </div>


        <div class="row">

          <div class="small-3 columns">
            <label for="marina-label" class="right inline">Marina Number:</label>
          </div>

          <div class="small-9 columns">
            <input type="text" name="marina_num" maxlength="1" value="<?php echo $marina_num; ?>"
            id="marina-label">
          </div>

        </div>

        <div class="row">

          <div class="small-3 columns">
            <label for="num-label" class="right inline">Slip Number:</label>
          </div>

          <div class="small-9 columns">
            <input type="text" name="slip_num" maxlength="2" value="<?php echo $slip_num; ?>"
            id="num-label">
          </div>

        </div>

        <div class="row">

          <div class="small-3 columns">
            <label for="length-label" class="right inline">Length:</label>
          </div>

          <div class="small-9 columns">
            <input type="text" name="length" value="<?php echo $length; ?>"
            id="length-label">
          </div>

        </div>

        <div class="row">

          <div class="small-3 columns">
            <label for="fee-label" class="right inline">Rental Fee:</label>
          </div>

          <div class="small-9 columns">
            <input type="text" name="rental_fee" value="<?php echo $rental_fee; ?>"
            id="fee-label">
          </div>

        </div>

        <div class="row">

          <div class="small-3 columns">
            <label for="name-label" class="right inline">Boat Name:</label>
          </div>

          <div class="small-9 columns">
            <input type="text" name="boat_name" value="<?php echo $boat_name; ?>"
            id="name-label">
          </div>

        </div>

        <div class="row">

          <div class="small-3 columns">
            <label for="type-label" class="right inline">Boat Type:</label>
          </div>

          <div class="small-9 columns">
            <input type="text" name="boat_type" value="<?php echo $boat_type; ?>"
            id="type-label">
          </div>

        </div>

        <div class="row">

        <div class="small-3 columns">
            <label for="own-label" class="right inline">Owner Number:</label>
          </div>

          <div class="small-9 columns">
            <input type="text" name="owner_num" value="<?php echo $owner_num; ?>"
            id="own-label">
          </div>

        </div>

        <div class="row">

          <div class="small-3 columns">
            <button class="form" name="insert-btn">Insert</button>
          </div>

          <div class="small-3 columns">
            <button class="form" name="modify-btn">Modify</button>
          </div>

          <div class="small-3 columns">
            <button class="form" name="delete-btn" onclick="return confirm('Are you sure you want to Delete?')">
              Delete
            </button>
          </div>

          <div class="small-3 columns">
            <button class="button alert" id="reset-btn" type="button">Reset</button>
          </div>

        </div>
      </form>

    </div>

    <a href="index.html" class="prev">Go Back</a>

    <footer class="service">
      &copy; Taylor
      <br />Last Updated 10/15/14
    </footer>


    <script src="../js/vendor/jquery.js"></script>
    <script src="../js/foundation.min.js"></script>
    <script>
      $(document).foundation();

      $(document).ready(function() {

        $('#reset-btn').click(function() {
          $(this).closest('form').find("input[type=text], textarea").val("");
        });


      });
    </script>
  </body>

  </html>
