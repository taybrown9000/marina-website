<?php
	$color = "red";

    $service_ID = trim($_POST['service_ID']);

    // get slip ID
    if (empty($_POST['slip_ID'])) {
      $errors[] = "The Slip ID field is blank";
    } else {
      $slip_ID = trim($_POST['slip_ID']);
    }

    if (empty($_POST['cat_num'])) {
      $errors[] = "The Category Number field is blank";
    } else {
      $cat_num = trim($_POST['cat_num']);
    }

    if (empty($_POST['desc'])) {
      $errors[] = "The Description field is blank";
    } else {
      $desc = trim($_POST['desc']);
    }

    if (empty($_POST['status'])) {
      $errors[] = "The Status field is blank";
    } else {
      $status = trim($_POST['status']);
    }

    if (empty($_POST['est_hrs'])) {
      $errors[] = "The Estimated Hours field is blank";
    } else {
      $est_hrs = trim($_POST['est_hrs']);
    }

    if (empty($_POST['spent_hrs'])) {
      $errors[] = "The Spent Hours field is blank";
    } else {
      $spent_hrs = trim($_POST['spent_hrs']);
    }

    if (empty($_POST['date'])) {
      $errors[] = "The Next Service Date field is blank";
    } else {
      $date = trim($_POST['date']);
    }
?>