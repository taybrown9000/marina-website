<?php
	$color = "red";

    // get owner number
    $owner_num = trim($_POST['owner_num']);

    // Check for a last name:
    if (empty($_POST['last_name'])) {
      $errors[] = "The Last Name field is blank";
    } else {
      $last_name = trim($_POST['last_name']);
    }

    // Check for a first name:
    if (empty($_POST['first_name'])) {
      $errors[] = "The First Name field is blank";
    } else {
      $first_name = trim($_POST['first_name']);
    }

    // Check for an address:
    if (empty($_POST['address'])) {
      $errors[] = "The Address field is blank";
    } else {
      $address = trim($_POST['address']);
    }

    // Check for a city:
    if (empty($_POST['city'])) {
      $errors[] = "The City field is blank";
    } else {
      $city = trim($_POST['city']);
    }

    // Check for a state:
    if (empty($_POST['state'])) {
      $errors[] = "The State field is blank";
    } else {
      $state = trim($_POST['state']);
    }

    // Check for a zipcode:
    if (empty($_POST['zipcode'])) {
      $errors[] = "The Zip Code field is blank";
    } else {
      $zipcode = trim($_POST['zipcode']);
    }
?>