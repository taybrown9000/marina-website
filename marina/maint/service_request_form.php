<!doctype html>
<html class="no-js" lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Service Request Maintenance</title>
  <link rel="stylesheet" href="../css/foundation.css" />
  <link rel="stylesheet" href="../css/app.css" />
  <script src="../js/vendor/modernizr.js"></script>
</head>

<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // initialize error array
  $errors = array();

    // retrieve function
  if (isset($_POST['retrieve-btn'])) {

    $service_ID = trim($_POST['service_ID']);

    // connect to database
    require('mysqli_connect.php');

    // make the query
    $q = "SELECT * FROM SERVICE_REQUEST WHERE SERVICE_ID = '$service_ID'";
    $r = @mysqli_query($dbc, $q);

      // check if the result was successful
    if (mysqli_num_rows($r) == 1) {
      $row = mysqli_fetch_array($r, MYSQLI_ASSOC);

      $service_ID = $row['SERVICE_ID'];
      $slip_ID = $row['SLIP_ID'];
      $cat_num = $row['CATEGORY_NUM'];
      $desc = $row['DESCRIPTION'];
      $status = $row['STATUS'];
      $est_hrs = $row['EST_HOURS'];
      $spent_hrs = $row['SPENT_HOURS'];
      $date = $row['NEXT_SERVICE_DATE'];

      $success = "Data successfully retrieved!\n" .
        "Service ID: " . $service_ID . "\n" .
        "Slip ID: " . $slip_ID . "\n" .
        "Category Number: " . $cat_num . "\n" .
        "Description: " . $desc . "\n" .
        "Status: " . $status . "\n" .
        "Estimated Hours: " . $est_hrs . "\n" .
        "Spent Hours: " . $spent_hrs . "\n" .
        "Next Service Date: " . $date . "\n";
      $color = "green";
    }
    else {
      $errors[] = "That Service ID doesn't seem to exist...Did you type it in correctly?";
      $color = "red";
    }
  } // end of retrieve function

  // insert function
  if (isset($_POST['insert-btn'])) {

    require('verify_service_request.php');

    // if everything's OK
    if (empty($errors)) {

      // connect to database
      require('mysqli_connect.php');

      //check if marina number exists already
      $q = "SELECT SERVICE_ID FROM SERVICE_REQUEST WHERE SERVICE_ID = '$service_ID'";
      $r = @mysqli_query($dbc, $q);

      // if the row is not found, continue
      if (mysqli_num_rows($r) == 0) {

      $q = "INSERT INTO SERVICE_REQUEST (SERVICE_ID, SLIP_ID, CATEGORY_NUM, DESCRIPTION, 
            STATUS, EST_HOURS, SPENT_HOURS, NEXT_SERVICE_DATE) VALUES ('$service_ID', '$slip_ID', 
            '$cat_num', '$desc', '$status', '$est_hrs', '$spent_hrs', '$date')";

      // run the query
        $r = @mysqli_query($dbc, $q);

      // if it ran OK
        if ($r) {
          $success = "Data successfully inserted!\n" .
            "Service ID: " . $service_ID . "\n" .
            "Slip ID: " . $slip_ID . "\n" .
            "Category Number: " . $cat_num . "\n" .
            "Description: " . $desc . "\n" .
            "Status: " . $status . "\n" .
            "Estimated Hours: " . $est_hrs . "\n" .
            "Spent Hours: " . $spent_hrs . "\n" .
            "Next Service Date: " . $date . "\n";

          $color = "green";

        } else {
          $errors[] = "There was an error inserting the data into the database.\nWe apologize for any inconvenience.";
          $color = "red";
        }

      // thing already exists
      } 
    } else {
      if (empty($errors)) {
          $errors[] = "The Service ID you've entered already exists in the database.";
          $color = "red";
      }
    }
  } // end of insert function

  // modify function
  if (isset($_POST['modify-btn'])) {

    require('verify_service_request.php');

    // if everything's OK
    if (empty($errors)) {

      // connect to database
      require('mysqli_connect.php');

      $q = "SELECT SERVICE_ID FROM SERVICE_REQUEST WHERE SERVICE_ID = '$service_ID'";
      $r = @mysqli_query($dbc, $q);

      // if the row is found, continue
      if (mysqli_num_rows($r) == 1) {

        $q = "UPDATE SERVICE_REQUEST SET SERVICE_ID='$service_ID', SLIP_ID='$slip_ID', 
        CATEGORY_NUM='$cat_num', DESCRIPTION='$desc', STATUS='$status', EST_HOURS='$est_hrs',
        SPENT_HOURS='$spent_hrs', NEXT_SERVICE_DATE='$date'
        WHERE SERVICE_ID = '$service_ID'";  

      // run the query
        $r = @mysqli_query($dbc, $q);

      // if it ran OK
        if ($r) {
          $success = "Data successfully modified!\n" .
            "Service ID: " . $service_ID . "\n" .
            "Slip ID: " . $slip_ID . "\n" .
            "Category Number: " . $cat_num . "\n" .
            "Description: " . $desc . "\n" .
            "Status: " . $status . "\n" .
            "Estimated Hours: " . $est_hrs . "\n" .
            "Spent Hours: " . $spent_hrs . "\n" .
            "Next Service Date: " . $date . "\n";

          $color = "green";

        } else {
          $errors[] = "There was an error replacing the data in the database.\nWe apologize for any inconvenience.";
          $color = "red";
        }

      // the data doesn't exist in the table
      } else {
        $errors[] = "The data was not found in the table...Did you type the Service ID in correctly?";
        $color = "red";
      }   
    } 
  } // end of modify function


  // delete function
  if (isset($_POST['delete-btn'])) {

    // get marina number
    $service_ID = trim($_POST['service_ID']);

    // connect to database
    require('mysqli_connect.php');

    // make the query
    $q = "SELECT * FROM SERVICE_REQUEST WHERE SERVICE_ID = '$service_ID'";
    $r = @mysqli_query($dbc, $q);

    // check if the result was successful
    if (mysqli_num_rows($r) == 1) {

      $q = "DELETE FROM SERVICE_REQUEST WHERE SERVICE_ID = '$service_ID'";
      $r = @mysqli_query($dbc, $q);

      // if it ran OK
      if ($r) {
        $success = "Data successfully deleted!\n" .
        "Service ID: " . $service_ID;

        $color = "green";

      } else {
        $errors[] = "There was an error deleting the data from the database.\nWe apologize for any inconvenience.";
        $color = "red";
      }

    // not found 
    } else {
      $errors[] = "The specified Service ID was not found in the database. Unable to delete.";
        $color = "red";
    }
  }

    // close db connection
  mysqli_close($dbc);
}

?>

<body>

  <a class="pic" href="../index.html"><img src="../img/marina.jpg" /></a>
  <a class="pic" href="../index.html"><img href="../index.html" src="../img/marina.jpg" class="right"/></a>
  <header>
    <h1>Brown Marina</h1>
  </header>

  <h2>Service Request Table</h2>
  <div class="page form">

    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id="myForm" data-abide>

      <!-- output textarea -->
      <div class="row">
        <div class="small-12 columns">
          <label class="inline">Output
            <textarea id="textarea" readonly class="error" style="color: <?php echo $color; ?>">
              <?php 
            // check if the operation was successful
              if (isset($success)) {
                echo "- $success";
              } else {
                foreach ($errors as $msg) {
                  echo "- $msg\n";
                }
              }
              ?>
            </textarea></label>
          </div>
        </div>


        <div class="row">
          <div class="small-12 columns">

            <!-- label -->
            <div class="small-3 columns">
              <label for="id-label" class="right inline">Service ID:</label>
            </div>

            <!-- text input -->
            <div class="row collapse">
              <div class="small-9 columns">

                <div class="small-9 columns">

                  <input type="text" id="id-label" name="service_ID"
                  value="<?php echo $_POST['service_ID']; ?>" required pattern="number">

                  <small class="error">Please enter a valid Service ID</small>
                </div>

                <!-- postfix button -->
                <div class="small-3 columns">
                  <button class="button postfix" name="retrieve-btn">Retrieve</button>
                </div>
              </div>
            </div>
          </div>
        </div>


        <div class="row">

          <div class="small-3 columns">
            <label for="sid-label" class="right inline">Slip ID:</label>
          </div>

          <div class="small-9 columns">
            <input type="text" name="slip_ID" value="<?php echo $slip_ID; ?>"
            id="sid-label">
          </div>

        </div>

        <div class="row">

          <div class="small-3 columns">
            <label for="cat-label" class="right inline">Category Number:</label>
          </div>

          <div class="small-9 columns">
            <input type="text" name="cat_num" value="<?php echo $cat_num; ?>"
            id="cat-label">
          </div>

        </div>

        <div class="row">

          <div class="small-3 columns">
            <label for="desc-label" class="right inline">Description:</label>
          </div>

          <div class="small-9 columns">
            <textarea name="desc" id="desc-label"><?php echo $desc; ?></textarea>
          </div>

        </div>

        <div class="row">

          <div class="small-3 columns">
            <label for="status-label" class="right inline">Status:</label>
          </div>

          <div class="small-9 columns">
            <textarea name="status" id="status-label"><?php echo $status; ?></textarea>
          </div>

        </div>

        <div class="row">

          <div class="small-3 columns">
            <label for="est-label" class="right inline">Estimated Hours:</label>
          </div>

          <div class="small-9 columns">
            <input type="text" name="est_hrs" value="<?php echo $est_hrs; ?>"
            id="est-label">
          </div>

        </div>

        <div class="row">

          <div class="small-3 columns">
            <label for="spent-label" class="right inline">Spent Hours:</label>
          </div>

          <div class="small-9 columns">
            <input type="text" name="spent_hrs" value="<?php echo $spent_hrs; ?>"
            id="spent-label">
          </div>

        </div>

        <div class="row">

        <div class="small-3 columns">
            <label for="date-label" class="right inline">Next Service Date:</label>
          </div>

          <div class="small-9 columns">
            <input type="text" name="date" value="<?php echo $date; ?>"
            id="date-label">
          </div>

        </div>

        <div class="row">

          <div class="small-3 columns">
            <button class="form" name="insert-btn">Insert</button>
          </div>

          <div class="small-3 columns">
            <button class="form" name="modify-btn">Modify</button>
          </div>

          <div class="small-3 columns">
            <button class="form" name="delete-btn" onclick="return confirm('Are you sure you want to Delete?')">
              Delete
            </button>
          </div>

          <div class="small-3 columns">
            <button class="button alert" id="reset-btn" type="button">Reset</button>
          </div>

        </div>
      </form>

    </div>

    <a href="index.html" class="prev">Go Back</a>


    <footer class="service">
      &copy; Taylor
      <br />Last Updated 10/15/14
    </footer>

    <script src="../js/vendor/jquery.js"></script>
    <script src="../js/foundation.min.js"></script>
    <script>
      $(document).foundation();

      $(document).ready(function() {

        $('#reset-btn').click(function() {
          $(this).closest('form').find("input[type=text], textarea").val("");
        });


      });
    </script>
  </body>

  </html>
