<?php
	$color = "red";

    // get slid ID
    $slip_ID = trim($_POST['slip_ID']);

    // get marina number
    if (empty($_POST['marina_num'])) {
      $errors[] = "The Marina Number field is blank";
    } else {
      $marina_num = trim($_POST['marina_num']);
    }

    // Check for a slip number:
    if (empty($_POST['slip_num'])) {
      $errors[] = "The Slip Number field is blank";
    } else {
      $slip_num = trim($_POST['slip_num']);
    }

    // Check for a length:
    if (empty($_POST['length'])) {
      $errors[] = "The length field is blank";
    } else {
      $length = trim($_POST['length']);
    }

    // Check for an rental_fee:
    if (empty($_POST['rental_fee'])) {
      $errors[] = "The Rental Fee field is blank";
    } else {
      $rental_fee = trim($_POST['rental_fee']);
    }

    // Check for a boat_name:
    if (empty($_POST['boat_name'])) {
      $errors[] = "The Boat Name field is blank";
    } else {
      $boat_name = trim($_POST['boat_name']);
    }

    // Check for a boat type:
    if (empty($_POST['boat_type'])) {
      $errors[] = "The Boat Type field is blank";
    } else {
      $boat_type = trim($_POST['boat_type']);
    }

    // Check for a owner num:
    if (empty($_POST['owner_num'])) {
      $errors[] = "The Zip Code field is blank";
    } else {
      $owner_num = trim($_POST['owner_num']);
    }
?>